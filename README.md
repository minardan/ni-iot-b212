# Ukázková struktura projektu

## Popis

Zadaním je detekcia pohybu pomocou PIR senzoru pripojeného k ESP a následné poslanie údajov do Home Assistenta. 
Využijem PIR senzoru, ktorý dokáže detekovať pohyb v danej oblasti. Daná informácia o pohybe sa pošle do Home Assistenta 
a ten ju zobrazí. Následne túto informáciu využijem k rozsvieteniu LED diody a k zaslaniu upozornenia.
## Využívané technologie

* HomeAssistant
* ESP8266
* PIR AM312 
* LED dioda
* Nepájivé pole

## Dokumentace

doc - hlavný adresár -> zaver - Výsledné prvky práce

Zdroje:
https://esphome.io/guides/getting_started_hassio.html
   https://www.home-assistant.io/getting-started/
